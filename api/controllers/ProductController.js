/**
 * ProductController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const { Product } = require('../connector/db');
const getProduct = async function (req, res) {
  try {
    const product = await Product.findAll();
    res.ok({ product });
  }
  catch (err) {
    return res.json({
      code: 500,
      message: err
    });
  }
};

module.exports = {
  getProduct
};

