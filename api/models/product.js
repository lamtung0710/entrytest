module.exports = (sequelize, type) => {
  return sequelize.define('products', {
    productName: type.STRING,
    price: type.DECIMAL
  });
};
