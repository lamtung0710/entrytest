const Sequelize = require('sequelize');
const _ = require('lodash');
const productModel = require('../models/product');

const Conn = new Sequelize(
  process.env.DB_NAME || 'EntryTest',
  process.env.USER_NAME || 'postgres',
  process.env.PASSWORD || 'abc@12345',
  {
    dialect: 'postgres',
    host: process.env.HOST || 'localhost',
    logging: true
  }
);
const Product = productModel(Conn, Sequelize);

module.exports = {
  Product
};
